#! /usr/bin/env python

#################################################################
# Written by Charles Epstein, Dec 2014
# Calculates sums of conductances for various vacuum components
# Base units: cm, torr
#################################################################

from __future__ import division
from numpy import *
from PyVacuum import *

#################################################################
# DL Stuff
#################################################################

def makePumpOut(length=100.):	#pump-out layout
	S = Pump(820.) 				#Pump
	Spl = Tube(19.77,23.00)		#Spool
	GV = Tube(19.77,8.43)		#Gate Valve
	L = Tube(19.77,length)		#Tube length
	PumpOut = Chain([L,GV,Spl,S])
	PumpOut.setTee()
	return PumpOut

def makePumpOutSp(s=820,length=100.):	#pump-out layout
	S = Pump(s) 				#Pump
	Spl = Tube(19.77,23.00)		#Spool
	GV = Tube(19.77,8.43)		#Gate Valve
	L = Tube(19.77,length)		#Tube length
	PumpOut = Chain([L,GV,Spl,S])
	PumpOut.setTee()
	# print "pump conductance",PumpOut.c()
	return PumpOut

def makeBeamPipe(length=100.):
	return Tube(7.14,length)

def makeStraw(dia=0.2,length=3,pres=3.):
	return TransitionalTube(dia,length,pres)

def makeVac():
	return Tube(30.,73.5)

def makeDump():
	return Tube(2.,49.)

def makeRoots():
	S = Pump(60)
	RP = Chain([S])
	RP.setTee()
	return RP

def DL():
	Temp = 300.0
	SetGasAttr(T=Temp,M=2.016)
	thk = 4e19		#cm^-2
	length = 60. 	#cm
	n = 0.5*thk/length	#0.5 because H2
	k = 62.36367*1e3/6.022e23	#L torr mol-1 to cm^3 torr n-1
	P0 = n*k*Temp	#calc required pressure

	print "############################# INPUT ################################"
	print "Input target thickness \t\t=",thk,"cm^-2"
	print "Input target length \t\t=",length,"cm"
	print "############################# OUTPUT ###############################"
	print "Desired target Pressure \t=",P0,"torr"



	PumpOut = makePumpOut(100)
	Straw = makeStraw(pres=P0)
	RP = makeRoots()

	#Make Upstream and Downstream geometries

	if 0:#3 stages with better pumps
		DL_US = Chain([Straw,makeBeamPipe(50),makePumpOutSp(s=2000,length=100),Tube(1.*inch,150),PumpOut,Tube(1.*inch,150),PumpOut])
		DL_DS = Chain([Straw,makePumpOutSp(s=2000,length=100),makeVac(),makeDump(),PumpOut,Tube(1.*inch,150),PumpOut])
	if 1:#3 stages of regular pumps
		DL_US = Chain([Straw,makeBeamPipe(50),PumpOut,Tube(2.*inch,200),PumpOut,Tube(2.*inch,200),PumpOut])
		DL_DS = Chain([Straw,PumpOut,makeVac(),makeDump(),PumpOut,makeBeamPipe(200),PumpOut])
	if 0:#4 Stages of pumping
		DL_US = Chain([Straw,makeBeamPipe(50),PumpOut,Tube(1.5*inch,100),PumpOut,Tube(1.5*inch,100),PumpOut,Tube(1.5*inch,100),PumpOut])
		DL_DS = Chain([Straw,PumpOut,makeVac(),makeDump(),PumpOut,makeBeamPipe(100),PumpOut,Tube(1.5*inch,100),PumpOut])

	CTOT = DL_US.c() + DL_DS.c()	#total conductance
	Q0 = P0*CTOT	#req input flow

	print "Required input flow \t\t=",Q0,"torr l/s"
	print "####################################################################"

	Q0U = Q0*DL_US.c()/CTOT	#Flow into upstream half
	Q0D = Q0*DL_DS.c()/CTOT	#Flow into downstream half

	#Calculate pressures at pump stages
	PU,QU = calcPres(DL_US,P0,Q0U)
	PD,QD = calcPres(DL_DS,P0,Q0D)

	print "Pressures Along Upstream Side:"
	for i in range(len(PU)):
		print "\t Pressure at Stage",i,"\t=",PU[i],"torr"

	print "Pressures Along Downstream Side:"
	for i in range(len(PD)):
		print "\t Pressure at Stage",i,"\t=",PD[i],"torr"
	print "############################# DONE #################################"

#Run it!
DL()
