#! /usr/bin/env python

#################################################################
# Written by Charles Epstein, Dec 2014
# Calculates sums of conductances for various vacuum components
# Base units: cm, torr
#################################################################

from __future__ import division

from numpy import *

inch = 2.54

def invSum(l):		#Sum by inverting sum of inverses
	if len(l)==0:
		return 0
	else:
		return 1./sum(1./i for i in l)

def SetGasAttr(T=300.0,M=2.016):
	ConductanceObj.T = T
	ConductanceObj.M = M


class ConductanceObj:	#Parent conductance class
	def __init__(self):
		self.conductance = None
		self.tee = None
	def c(self):			#return the conductance, whichever way
		return self.conductance
	def isTee(self):		#Is this a tee-off, ie, a pumpout connected in parallel?
		return self.tee
	def setTee(self):
		self.tee = True
	def unsetTee(self):
		self.tee = False

class Tube(ConductanceObj):	#Molecular flow tube
	def __init__(self,dia,length):
		ConductanceObj.__init__(self)
		self.conductance = 3.81*sqrt(self.T/self.M)*(dia**3)/length
		self.tee = False

class Pump(ConductanceObj):	#Pump = general conductance
	def __init__(self,speed):
		ConductanceObj.__init__(self)
		self.conductance = speed
		self.tee = False

class TransitionalTube(ConductanceObj):	#Phenomenological transitional flow tube, Tison 1993
	def __init__(self,dia,length,pres):
		ConductanceObj.__init__(self)
		self.d = dia
		self.l = length
		self.p = pres
		self.mol = Tube(dia,length) #Same tube in molecular flow region
		self.calc()
		self.tee = False
	def calc(self):
		mu = 0.00000847 #viscosity in Pa s  = 84.7 micropoise for hydrogen
		P = self.p*133.322368 #pressure in Pa, converting from torr
		mfp = 116.4*(mu/P)*sqrt(self.T/self.M) #Mean free path, m
		a = self.d/200. #radius in m (from dia in cm)
		self.conductance = self.mol.c()*(0.1472*a/mfp + (1.+3.50*a/mfp)/(1.+5.17*a/mfp))
	def molC(self):
		return self.mol.c()

class Chain(ConductanceObj):		#Chain of conductance objects with proper addition
	def __init__(self, chunks_init =[], tee_init = False):
		self.chunks = chunks_init
		self.tee = tee_init
	def __len__(self):
		return len(self.chunks)
	def __getitem__(self,key):
		return self.chunks[key]
	def __getslice__(self,i,j):
		return Chain(self.chunks[i:j])
	def __iter__(self):
		return iter(self.chunks)
	def extend(self, chunk):
		self.chunks.append(chunk)
	def c(self):			#Go through and add up the conductances with proper branching
		c_list = []
		for chunk in reversed(self.chunks):
			if chunk.isTee():
				c_list = [chunk.c()+invSum(c_list)]
			else:
				c_list.append(chunk.c())

		return invSum(c_list)

def calcPres(geomChain,P0,Q0):	#Calc pressure at branches
	P = [P0]
	Q = [Q0]
	for i in range(len(geomChain)):
		if geomChain[i].isTee():
			cTee = geomChain[i].c()	#conductance of tee
			cRest = geomChain[i+1:].c()	#conductance of rest of system
			qRest = Q[-1]*cRest/(cRest+cTee)	#Q into rest of system
			qTee = Q[-1]*cTee/(cRest+cTee)		#Q into tee
			Q.append(qRest)			#(keep track)
			P.append(qTee/cTee)		#Pressure at tee (equiv to qRest/cRest)
	return P,Q
